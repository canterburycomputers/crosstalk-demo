Crossbar Demo
-------------

A demo project to simply show a different way to django (using crossbar.io + celery)


Installing
----------

Install dependencies

    apt-get install libpq-dev python-dev rabbitmq-server        
    pip install -r server/requirements.txt


Install postgresql (ideally for production this should be on a seperate machine)

    apt-get install postgresql postgresql-contrib
    #on ubuntu 14.04 postgres usually complains about the locale set on first install
    dpkg-reconfigure locales
    export LC_ALL="en_US.UTF-8"
    pg_createcluster 9.3 nameofclusterwhateveryouwant --start

    su - postgres

    createdb -U postgres crosstalkdemo
    psql -U postgres -c "GRANT ALL PRIVILEGES ON DATABASE crosstalkdemo TO postgres;"
    psql -U postgres -c "alter role postgres password 'postgres'"

    exit


Initial Setup
----------

Modify server/_env/env.json to match your database settings, email settings etc

Create an admin user

    cd server/
    ./manage.py migrate
    ./manage.py createsuperuser

(optional) Load demo fixtures (contains WAMP-CRA credentials for authenticated front end):

    ./manage.py loaddata _fixtures_demo/*



Running
-------

Crossbar will run the WAMP router + django WSGI + celeryd + celerycam + static files hosting. From the server/ directory run:

    crossbar start

By default crossbar will listen to localhost:8080

http://localhost:8080/admin
http://localhost:8080/test (runnable js examples)

What Next?
----------

Add some celery tasks and modify server/crosstalk_settings.py

Change server port and/or add a tls certificate to server/.crossbar/config.json

To-Do
-----

Vagrantfile (& maybe Docker)


TLDR
----

Important Files:
    
    server/_env/env.json (env settings, database, email etc)
    server/djangoadmin/settings.py (django settings)
    server/.crossbar/config.json (crossbar settings; includes ssl/tls settings, server port & wamp permissions)
    server/crosstalk_settings.py (include your celery tasks here to have them callable from wamp)