// authenticate using authid "peter", and using a salted password
            var user = "peter";
            var key = autobahn.auth_cra.derive_key("secret1", "salt123", 100, 16);

 /* This is the object holding the logic
         for our buy/donate button. .*/
      var buy = {
      busy: false,
      buyCoffee: function(){
            // placeholder since we bind to this before the correct function is set
         }
      };


         // the URL of the WAMP Router (Crossbar.io)
         //
         var wsuri;
         if (document.location.origin === "null" || document.location.origin === "file://") {
            wsuri = "ws://127.0.0.1:8080/ws";

         } else {
            wsuri = (document.location.protocol === "http:" ? "ws:" : "wss:") + "//" +
                        document.location.host + "/ws";
         }

         // this callback is fired during WAMP-CRA authentication
         //
         function onchallenge (session, method, extra) {

            console.log("onchallenge", method, extra);

            if (method === "wampcra") {

               console.log("authenticating via '" + method + "' and challenge '" + extra.challenge + "'");

               return autobahn.auth_cra.sign(key, extra.challenge);

            } else {
               throw "don't know how to authenticate using '" + method + "'";
            }
         }

         // the WAMP connection to the Router
         //
         var connection = new autobahn.Connection({
            url: wsuri,
            realm: "realm1",
            // the following attributes must be set of WAMP-CRA authentication
            //
            authmethods: ["wampcra"],
            authid: user,
            onchallenge: onchallenge
         });


         // timers
         //
         var t1, t2;


         // fired when connection is established and session attached
         //
         connection.onopen = function (session, details) {

            console.log("Connected");

            // SUBSCRIBE to a topic and receive events
            //

            // function on_counter (args) {
            //    var counter = args[0];
            //    console.log("on_counter() event received with counter " + counter);
            // }
            // session.subscribe('com.example.oncounter', on_counter).then(
            //    function (sub) {
            //       console.log('subscribed to topic');
            //    },
            //    function (err) {
            //       console.log('failed to subscribe to topic', err);
            //    }
            // );

         /* We bind the button click event to
         a call to the play() function on
         the remote player. The uuid allows
         us to only send the event to the
         right player. */
      buy.buyCoffee = function() {
        
          session.call('com.example.call_authenticated_celery_task');
          console.log("calling call_authenticated_celery_task");
        
      };
            // PUBLISH an event every second
            //
            // t1 = setInterval(function () {

            //    session.publish('com.example.onhello', ['Hello from JavaScript (browser)']);
            //    console.log("published to topic 'com.example.onhello'");
            // }, 1000);




            // CALL a remote procedure every second
            //
         //    var x = 0;

         //    t2 = setInterval(function () {

         //       session.call('com.example.add2', [x, 18]).then(
         //          function (res) {
         //             console.log("add2() result:", res);
         //          },
         //          function (err) {
         //             console.log("add2() error:", err);
         //          }
         //       );

         //       x += 3;
         //    }, 1000);
          };


         // fired when connection was lost (or could not be established)
         //
         connection.onclose = function (reason, details) {
            console.log("Connection lost: " + reason);
            if (t1) {
               clearInterval(t1);
               t1 = null;
            }
            if (t2) {
               clearInterval(t2);
               t2 = null;
            }
         }


         // now actually open the connection
         //
         connection.open();
