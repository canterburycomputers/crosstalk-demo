############
# SETTINGS #
############
CROSSBAR_CELERY_TASKS = [
{'app': 'demoapp.tasks', 'task': 'example_celery_task', 'cb_rpc': 'call_celery_task'},
{'app': 'demoapp.tasks', 'task': 'example_celery_task', 'cb_rpc': 'call_authenticated_celery_task'},
];
CROSSBAR_DOMAIN = 'com.example'