###############################################################################
##
##  Copyright (C) 2014, Tavendo GmbH and/or collaborators. All rights reserved.
##
##  Redistribution and use in source and binary forms, with or without
##  modification, are permitted provided that the following conditions are met:
##
##  1. Redistributions of source code must retain the above copyright notice,
##     this list of conditions and the following disclaimer.
##
##  2. Redistributions in binary form must reproduce the above copyright notice,
##     this list of conditions and the following disclaimer in the documentation
##     and/or other materials provided with the distribution.
##
##  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
##  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
##  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
##  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
##  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
##  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
##  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
##  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
##  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
##  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
##  POSSIBILITY OF SUCH DAMAGE.
##
###############################################################################

from twisted.internet.defer import inlineCallbacks

from os import environ

from autobahn.twisted.util import sleep
from autobahn.twisted.wamp import ApplicationSession, ApplicationRunner
from autobahn.wamp.exception import ApplicationError

#added for txpostgres
#from txpostgres import txpostgres
from twisted.python import log, util

import crosstalk_settings
CROSSBAR_CELERY_TASKS = crosstalk_settings.CROSSBAR_CELERY_TASKS
CROSSBAR_DOMAIN = crosstalk_settings.CROSSBAR_DOMAIN

from  more_itertools import unique_everseen

##this isn't very nice but it works.  cleanup!
ti = []
for t in CROSSBAR_CELERY_TASKS:
    ti.append(t.get('app')+'.'+t.get('task'))
ti_unique = list(unique_everseen(ti))
for import_task in ti_unique:
    i = import_task.split('.')
    imt = 'from '+i[0]+'.'+i[1]+' import '+i[2]
    exec(imt)


class AppSession(ApplicationSession):

    @inlineCallbacks
    def onJoin(self, details):

        ## REGISTER celery tasks as procedures for remote calling
        ##
        for t in CROSSBAR_CELERY_TASKS:
            setattr(self, t.get('cb_rpc'), lambda: getattr(eval(t.get('task')), 'delay')().addCallback(returnData))
            reg = yield self.register(eval('self.'+t.get('cb_rpc')), CROSSBAR_DOMAIN+'.'+t.get('cb_rpc'))
            print("procedure "+t.get('cb_rpc')+"() registered")

        def returnData(d):
            """
            Data handling function to be added as a callback: handles the
            data by returning the result
            """
            return d
