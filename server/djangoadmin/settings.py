"""
Django settings for djangoadmin project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
#from celery.schedules import crontab

from unipath import FSPath as Path
import os
import json

BASE_DIR = os.path.dirname(os.path.dirname(__file__))
BASE = Path(__file__).absolute().ancestor(2)

# ENVIRONMENT SPECIFICS SETTINGS in the file named sbextrader/env.json :

with open(BASE.child('_env').child('env.json')) as handle:
    ENV = json.load(handle)

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = ENV['secret']

# SECURITY WARNING: don't run with debug turned on in production!
if ENV['debug'] == "True":
    DEBUG = True
    TEMPLATE_DEBUG = True
else: 
    DEBUG = False
    TEMPLATE_DEBUG = False

ALLOWED_HOSTS = ENV['allowed_hosts']

# Application definition

INSTALLED_APPS = (
    'grappelli',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'djcelery',
    'wampcra_auth',
    'demoapp',
    'smuggler',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'djangoadmin.urls'

WSGI_APPLICATION = 'djangoadmin.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.sqlite3',
#         'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
#     }
# }

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': ENV.get('database', ''),                      # Or path to database file if using sqlite3.
        # The following settings are not used with sqlite3:
        'USER': ENV.get('db_user', ''),
        'PASSWORD': ENV.get('db_pass', ''),
        'HOST': '127.0.0.1', # Empty for localhost through domain sockets or '127.0.0.1' for localhost through TCP.
        'PORT': '', # Set to empty string for default.
    }
} 

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = 'static/'


# ------------ GRAPPELLI_ADMIN -------------#
GRAPPELLI_ADMIN_TITLE = ENV.get('title', '') 

# ----------------- CELERY -----------------#
import djcelery

CELERY_ACCEPT_CONTENT = ['json', 'pickle'] #pickle needed for some reason
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'

djcelery.setup_loader()

## Broker settings.
BROKER_URL = ENV['broker_url']

CELERY_RESULT_BACKEND = ENV['celerey_result_backend']

# CELERY Periodic tasks

# CELERYBEAT_SCHEDULE = {
#     # crontab(hour=0, minute=0, day_of_week='saturday')
#     'update_openexchange_rates_task': {
#         'task': 'exchange_rates.tasks.update_openexchange_rates_task', 
#         'schedule': crontab(minute='*/10')
#     },
#     }


# }
