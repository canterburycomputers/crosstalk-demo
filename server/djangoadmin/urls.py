from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings


urlpatterns = patterns(
    '',
    # Examples:
    # url(r'^$', 'djangoadmin.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^grappelli/', include('grappelli.urls')), # grappelli URLS
    
)
if settings.DEBUG:
    urlpatterns += patterns('',
    (r'^admin/', include('smuggler.urls')),  # before admin url patterns!
    )