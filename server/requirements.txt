crossbar
#txpostgres
txCelery
django>=1.7,<1.9
psycopg2
django-grappelli
grappelli-nested-inlines
django-modeladmin-reorder
django-extensions
django-reversion
django-celery
unipath
more-itertools
#optional, only for debug mode
django-smuggler
