# -*- coding: utf-8 -*-
from __future__ import absolute_import
from celery.utils.log import get_task_logger

#for testing
from time import sleep

#for crossbar(twisted)
from txcelery.defer import CeleryClient

from djangoadmin.celery import app as celery_app

@CeleryClient
@celery_app.task
def example_celery_task():
    print "running celery task"
    sleep(5)
    return "result from celery task"