from django.db import models
from django.utils.translation import ugettext_lazy as _

# Create your models here.
from autobahn.wamp.auth import generate_wcs, derive_key

import uuid

class WampCraCredentials(models.Model):
    """
    This model is where all the WAMP-CRA authentication information is stored
    It might be prudent to obscure the secret in the admin 
    """
    class Meta:
        verbose_name        = _('WAMP-CRA credential')
        verbose_name_plural = _('WAMP-CRA credentials')

    authid = models.CharField(max_length=32)
    secret = models.CharField(max_length=255, default='', blank=True)
    secret_clear = models.CharField(max_length=255, default='', blank=True)
    role = models.CharField(max_length=64)
    salt = models.CharField(max_length=64, blank=True)
    iterations = models.IntegerField(default=1000)
    keylen = models.IntegerField(default=32)

    def __unicode__(self):
        return u"Auth for %s" % (self.authid)

    def save(self, *args, **kwargs):
        if not self.salt:
            self.salt = self.generate_salt()
        if not self.secret and not self.secret_clear:
            self.secret_clear = self.generate_secret()
            self.secret = self.derive_cra_key(self.salt, self.secret_clear, self.iterations, self.keylen)
        else:
            self.secret = self.derive_cra_key(self.salt, self.secret_clear, self.iterations, self.keylen)

        return super(WampCraCredentials, self).save(*args, **kwargs)

    def generate_salt(self):
        # Get a random UUID.
        return uuid.uuid4().hex

    def generate_secret(self):
        # Generate random secret (32 char length).
        return generate_wcs(32)
            
    def derive_cra_key(self, salt, secret, iterations, keylen):
        # Derive the key
        return derive_key(secret.encode('utf8'), salt.encode('utf8'), iterations, keylen).decode('ascii')



def create_api_key(sender, **kwargs):
    """
    A signal for hooking up automatic ``WampCraCredentials`` creation.
    """
    if kwargs.get('created') is True:
        WampCraCredentials.objects.create(user=kwargs.get('instance'))

