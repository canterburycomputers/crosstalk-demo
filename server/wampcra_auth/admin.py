from django.contrib import admin

from django.utils.translation import ugettext_lazy as _
from wampcra_auth.models import WampCraCredentials
# Register your models here.

class WampCraCredentialsAdmin(admin.ModelAdmin):
    readonly_fields            = ('secret', )
    fieldsets                  = (
    (_('Required'), {'fields': ('authid', 'role', 'secret_clear',)}),
    (_('Auto-Calculated (give this to client)'), {'fields': ('secret',)}),
    (_('Defaults Ok'), {'fields': ('salt', 'iterations', 'keylen', )}),
    )
    pass


admin.site.register(WampCraCredentials, WampCraCredentialsAdmin)
