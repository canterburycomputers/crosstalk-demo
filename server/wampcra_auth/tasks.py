# -*- coding: utf-8 -*-
from __future__ import absolute_import
from celery.utils.log import get_task_logger

#for crossbar(twisted)
from txcelery.defer import CeleryClient

from djangoadmin.celery import app as celery_app

from wampcra_auth.models import WampCraCredentials

#user auth task (for crossbar authenticator)

@CeleryClient
@celery_app.task
def authenticate_task(authid):
    print "authenticating"
    print authid
    try:
        u = WampCraCredentials.objects.get(authid=authid)
    except WampCraCredentials.DoesNotExist:
        return "nouser"
    print "thedict"
    result = { 'secret': u.secret, 'role': u.role, 'salt': u.salt, 'iterations': u.iterations, 'keylen': u.keylen }
    return dict(result)
