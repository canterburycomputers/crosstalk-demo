# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='WampCraCredentials',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('secret', models.CharField(default=b'', max_length=255)),
                ('role', models.CharField(max_length=64)),
                ('salt', models.CharField(max_length=64)),
                ('iterations', models.IntegerField()),
                ('keylen', models.IntegerField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
