# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wampcra_auth', '0002_wampcracredentials_authid'),
    ]

    operations = [
        migrations.AddField(
            model_name='wampcracredentials',
            name='secret_clear',
            field=models.CharField(default=b'', max_length=255, blank=True),
        ),
        migrations.AlterField(
            model_name='wampcracredentials',
            name='iterations',
            field=models.IntegerField(default=1000),
        ),
        migrations.AlterField(
            model_name='wampcracredentials',
            name='keylen',
            field=models.IntegerField(default=32),
        ),
        migrations.AlterField(
            model_name='wampcracredentials',
            name='salt',
            field=models.CharField(max_length=64, blank=True),
        ),
        migrations.AlterField(
            model_name='wampcracredentials',
            name='secret',
            field=models.CharField(default=b'', max_length=255, blank=True),
        ),
    ]
